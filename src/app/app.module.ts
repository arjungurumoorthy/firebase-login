import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppFirebaseModule } from './app-firebase/app-firebase.module';
import { WindowService } from './common/window/window.service';
import { AuthService } from './common/auth/auth.service';
import { LoginOptionsComponent } from './components/login-options/login-options.component';
import { AngularFirestore } from 'angularfire2/firestore';
import { ProfileDetailsComponent } from './components/profile-details/profile-details.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomepageComponent } from './components/homepage/homepage.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginOptionsComponent,
    ProfileDetailsComponent,
    HeaderComponent,
    FooterComponent,
    HomepageComponent
    
  ],
  imports: [
    BrowserModule,
    AppFirebaseModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
    
    
    
  ],
  providers: [WindowService,AuthService,AngularFirestore],
  bootstrap: [AppComponent]
})
export class AppModule { }
