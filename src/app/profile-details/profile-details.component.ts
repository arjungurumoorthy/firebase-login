import { Component, OnInit } from '@angular/core';
import { AuthService } from '../common/auth/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { User } from '../common/core/core.module';
import {Phone} from '../common/core/phone/phone.module';
import { Observable } from 'rxjs';
import { UserInfo } from 'firebase';
@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.css']
})
export class ProfileDetailsComponent implements OnInit {
  phone:string;
  phoneNumber:any;
  user$: Observable<User>;
 

  constructor(public auth : AuthService,
    public afAuth: AngularFireAuth, public afs: AngularFirestore) { 
      
    
  }

  ngOnInit(): void {
    
  }
  change(event:any){
    
    this.auth.phone=event.target.value
    this.phone=event.target.value  
  }
  PhoneUpdate(){
    const uid=this.afAuth.auth.currentUser.uid
    const userRef: AngularFirestoreDocument<Phone> = this.afs.doc('Users/' + uid);
    const data :Phone = {
      phone: this.phone,
      }
      return userRef.set(data, { merge: true });
    
    
  }

}
