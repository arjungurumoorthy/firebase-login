import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginOptionsComponent } from './components/login-options/login-options.component';
import { ProfileDetailsComponent } from './components/profile-details/profile-details.component';
import { HomepageComponent } from './components/homepage/homepage.component';

const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' }, // for defaault redirecting
  { path: '', component: LoginOptionsComponent },
  { path: 'home', component: HomepageComponent },
  { path: 'Updateprofile', component: ProfileDetailsComponent }
];


@NgModule({

  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled', // Add options right here
  })],
  exports: [RouterModule]

})
export class AppRoutingModule { }
