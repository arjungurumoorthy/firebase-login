import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import * as firebase from 'firebase';
import { User } from '../core/core.module';
import { Router } from '@angular/router';


@Injectable()
export class AuthService {

  RoleType: string;
  user$: Observable<User>;
  phone: any;
  userid: string;
  user: firebase.User;



  constructor(public afAuth: AngularFireAuth,
    public afs: AngularFirestore,
    private router: Router) {

    this.user$ = this.afAuth.authState.pipe(switchMap(
      user => {
        if (user) {
          return this.afs.doc<User>('Users/' + user.uid).valueChanges();
        }
        else {
          return of(null);
        }
      }
    ))
    this.afAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.afs.collection('Users').get().toPromise().then((snapshot) =>
          snapshot.docs.forEach(doc => {
            if (doc.data().uid == this.afAuth.auth.currentUser.uid) {
              if (doc.data().phoneNumber == undefined) {
                this.router.navigateByUrl('Updateprofile')
                console.log(doc.data().phoneNumber + "docs")
              }
              else {
                this.router.navigateByUrl('home')
                console.log(doc.data().phoneNumber + "doc")
              }


            }
          }))
      }
    });
  }

  googleSignIn() {
    const provider = new firebase.auth.GoogleAuthProvider()
    return this.oAuthLogin(provider);
  }
  oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((Credential) => {
        this.updateUserData(Credential.user)
      })
  }

  public updateUserData(user) {

    const userRef: AngularFirestoreDocument<User> = this.afs.doc('Users/' + user.uid);

    const data: User = {
      uid: user.uid,
      email: user.email,
      photoURL: user.photoURL,
      name: user.displayName,
      roleType: this.RoleType
    }
    return userRef.set(data, { merge: true });
  }

  async signOut() {
    await this.afAuth.auth.signOut();
    this.router.navigateByUrl('')
    console.log("signedOut")
  }

}





