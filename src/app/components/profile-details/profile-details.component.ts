import { Component, OnInit, AfterViewInit, AfterContentInit, OnChanges } from '@angular/core';
import { AuthService } from '../../common/auth/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { User } from '../../common/core/core.module';
import { Phone } from '../../common/core/phone/phone.module';
import { Observable, config } from 'rxjs';
import { environment } from 'src/environments/environment';
import { app } from 'firebase';
import { FirebaseApp } from 'angularfire2';
import { AppFirebaseModule } from 'src/app/app-firebase/app-firebase.module';
import { Router } from '@angular/router';
@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.css']
})
export class ProfileDetailsComponent implements OnInit {
  phone: string;
  phoneNumber: any;
  user$: Observable<User>;
  uids: string;
  email: string;
  user: firebase.User;
  user_details: any[];
  constructor(public auth: AuthService,
    public afAuth: AngularFireAuth,
    public afs: AngularFirestore,
    private router: Router) {

    this.afAuth.user.subscribe((data => {
      this.user = data;
    }))
  }
  ngOnInit(): void {

    this.uids = this.auth.userid
    console.log("uids" + this.afAuth.auth)
  }
  change(event: any) {
    this.auth.phone = event.target.value
    this.phone = event.target.value
  }
  PhoneUpdate() {

    const uid = this.afAuth.auth.currentUser.uid
    const userRef: AngularFirestoreDocument<Phone> = this.afs.doc('Users/' + uid);

    const data: Phone = {
      phoneNumber: this.phone,
    }
    if (this.phone != '') {
      this.router.navigateByUrl('home')
    }
    return userRef.set(data, { merge: true });



  }
}
