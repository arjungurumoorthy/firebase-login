import { Component, OnInit, OnChanges } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/common/auth/auth.service';
import { auth } from 'firebase';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';


@Component({
  selector: 'app-login-options',
  templateUrl: './login-options.component.html',
  styleUrls: ['./login-options.component.css']
})
export class LoginOptionsComponent implements OnInit {
  siginInButton = true
  uids: string

  constructor(public auth: AuthService, public afAuth: AngularFireAuth) {

  }
  ngOnInit(): void {
    this.siginInButton = true
  }
  click(event: any) {
    this.auth.RoleType = event.target.value
    this.siginInButton = false
  }
  google() {
    this.auth.googleSignIn();
  }
}
